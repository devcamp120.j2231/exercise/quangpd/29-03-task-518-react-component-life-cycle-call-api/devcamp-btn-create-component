
import Display from "./Display";
const { Component } = require("react");
class Parent extends Component{
    constructor(props){
        super(props);

        this.state = {
            display : false
        }
    }

    onClick = () => {
        this.setState({
            display : true
        })
    }


    render(){
        return(

            <>
                <div className="row mt-5">
                    <div className="col-sm-4">
                       {this.state.display ? <Display /> : <button onClick={this.onClick} className="btn btn-primary">Create Component</button>}
                    </div>
                </div>
            </>
        )
    }
}

export default Parent